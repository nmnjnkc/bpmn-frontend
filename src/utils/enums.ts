export enum RoutePaths {
    INDEX = '/',
    CREATE_BPMN = '/create',
    DIAGRAM_BPMN = '/diagram',
    UPDATE_BPMN = '/update'
  }