import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { api } from "../../utils/axios.config";



interface DiagramState {
  isDeleted: boolean;
  isLoading: boolean;
  isSuccess: boolean;
  errorMessage: string | null;
  diagram: DiagramData | null,
  diagrams: DiagramData[],

}

export interface DiagramData {
  fileName?: string;
  authorId?: number;
  uploadDate?: string;
  bpmnDiagramFile?: File;
}

export const getAllDiagrams = createAsyncThunk(
  "diagrams/getAllDiagrams",
  async () => {
    const response = await api.get("");
    return response?.data;
  }
);

export const getDiagramById = createAsyncThunk(
  "diagrams/getDiagramById",
  async (fileId: string) => {
    const response = await api.get(`/${fileId}`);
    return response?.data;
  }
);

export const createDiagram = createAsyncThunk(
  "diagrams/createDiagram",
  async (data: DiagramData) => {  
    const formData = new FormData();
    formData.append("fileName", data.fileName || "");
    formData.append("authorId", String(data.authorId || ""));
    formData.append("uploadDate", data.uploadDate || "");
    formData.append("bpmnDiagramFile", data.bpmnDiagramFile || new File([], ""));

    const response = await api.post("", formData); 
    return response?.data;
  }
);

export const deleteDiagram = createAsyncThunk(
  "diagrams/deleteDiagram",
  async (fileId: string) => {
    const response = await api.delete(`/${fileId}`);
    return response?.data;
  }
);

export const updateDiagram = createAsyncThunk(
  "diagrams/updateDiagram",
  async ({ fileId, data }: { fileId: string; data: DiagramData }) => {
    const response = await api.put(`/${fileId}`, data);
    return response?.data;
  }
);

const diagramSlice = createSlice({
  name: "diagram",
  initialState: {
    isDeleted: false,
    isLoading: false,
    isSuccess: false,
    errorMessage: null,
    diagram: null,
  } as DiagramState,
  reducers: {
  },
  extraReducers: (builder) => {
    builder
      .addCase(getAllDiagrams.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getAllDiagrams.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.errorMessage = null;
        state.diagrams = action.payload //.content

      })
      .addCase(getAllDiagrams.rejected, (state, action) => {
        state.isLoading = false;
        state.isSuccess = false;
        state.errorMessage = action.error.message || "Failed to fetch data";
      })
      .addCase(getDiagramById.pending, (state) => {
        state.isLoading = true
        state.diagram = null

      })
      .addCase(getDiagramById.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true;
        state.errorMessage = null;
        state.diagram = action.payload
      })
      .addCase(getDiagramById.rejected, (state, action) => {
        state.isLoading = false
        state.errorMessage = action.error.message || "Failed to fetch data";
      })
      .addCase(createDiagram.pending, (state) => {
        state.isLoading = true
      })
      .addCase(createDiagram.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true;
        state.errorMessage = null;
        state.diagram = action.payload //?
      })
      .addCase(createDiagram.rejected, (state, action) => {
        state.isLoading = false
        state.errorMessage = action.error.message || "Failed to create diagram";
      })
      .addCase(deleteDiagram.pending, (state) => {
        state.isLoading = true
        state.isDeleted = false
      })
      .addCase(deleteDiagram.fulfilled, (state) => {
        state.isLoading = false
        state.isDeleted = true
      })
      .addCase(deleteDiagram.rejected, (state, action) => {
        state.isLoading = false
        state.errorMessage = action.error.message || "Failed to delete data";
      })
      .addCase(updateDiagram.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(updateDiagram.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.errorMessage = null;
        state.diagram = action.payload

      })
      .addCase(updateDiagram.rejected, (state, action) => {
        state.isLoading = false;
        state.isSuccess = false;
        state.errorMessage = action.error.message || "Failed to update diagram";
      });
  },
});

export default diagramSlice.reducer;
