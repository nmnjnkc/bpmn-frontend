import type { RootState } from "../store"

const diagramSelector = (state: RootState) => state.diagrams

export const selectDiagrams = (state: RootState) => diagramSelector(state)?.diagrams

export const selectDiagram = (state: RootState) => diagramSelector(state).diagram; 

export const selectDiagramIsLoading = (state: RootState) => diagramSelector(state).isLoading

export const selectDiagramIsSuccess = (state: RootState) => diagramSelector(state).isSuccess

export const selectDiagramIsDeleted = (state: RootState) => diagramSelector(state).isDeleted
