import React, { useEffect, useRef } from "react";
import BpmnModeler from "bpmn-js/lib/Modeler";
import "bpmn-js/dist/assets/bpmn-font/css/bpmn.css";
import "bpmn-js/dist/assets/diagram-js.css";
import "bpmn-js/dist/assets/bpmn-js.css";
import { useNavigate } from "react-router-dom";
import Button from "../Button/Button";
import "../../pages/CreateBpmnPage/CreateBpmnPage.scss";
import { RoutePaths } from "../../utils/enums";
import { DiagramData, createDiagram } from "../../redux/diagrams/diagrams.slice";
import { useAppDispatch } from "../../redux/store";

const BpmnModelerComponent: React.FC = () => {
  const containerRef = useRef<HTMLDivElement>(null!);
  const modelerRef = useRef<BpmnModeler | null>(null);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const initializeModeler = () => {
    if (!modelerRef.current) {
      modelerRef.current = new BpmnModeler({ container: containerRef.current });
      modelerRef.current.createDiagram();
    }
  };
 
  useEffect(() => {
    initializeModeler();
  }, []);

  const createBpmn = async () => {
    if (modelerRef.current) {
      try {
        const result = await modelerRef.current.saveXML({ format: true });

        const xmlBlob = new Blob([result.xml], { type: "text/xml" });

        const diagramData: DiagramData = {
          fileName: "Demo",
          authorId: 123, 
          bpmnDiagramFile: new File([xmlBlob], "diagram.bpmn")
        };

        await dispatch(createDiagram(diagramData));

        navigate(RoutePaths.INDEX);
      } catch (error) {

        console.error("Failed to save the BPMN diagram:", error);
      }
    }  };

  const cancelBpmn = () => {
    if (modelerRef.current) {
      modelerRef.current.destroy();
      modelerRef.current = null;
    }
    navigate(RoutePaths.INDEX);
  };

  return (
    <>
      <div
        ref={containerRef}
        style={{ height: "80vh", border: "1px solid #ccc" }}
      />

      <Button name={"Create"} clickHandler={createBpmn} />
      <Button name={"Cancel"} clickHandler={cancelBpmn} />
    </>
  );
};

export default BpmnModelerComponent;
