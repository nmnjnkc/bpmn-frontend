import React, { useEffect, useRef } from "react";
import BpmnViewer from "bpmn-js/lib/Viewer";
import "bpmn-js/dist/assets/bpmn-font/css/bpmn.css";
import "bpmn-js/dist/assets/diagram-js.css";
import "bpmn-js/dist/assets/bpmn-js.css";
import {
  selectDiagramIsLoading,
  selectDiagram,
} from "../../redux/diagrams/diagrams.selectors";
import { getDiagramById } from "../../redux/diagrams/diagrams.slice";
import { useAppDispatch, useAppSelector } from "../../redux/store";

const BpmnViewerComponent: React.FC<{ diagramId: string }> = ({
  diagramId,
}) => {
  const containerRef = useRef<HTMLDivElement>(null!);
  const viewerRef = useRef<BpmnViewer | null>(null);
  const dispatch = useAppDispatch();
  const isLoading = useAppSelector(selectDiagramIsLoading);
  const diagramXML = useAppSelector(selectDiagram);

  useEffect(() => {
    dispatch(getDiagramById(diagramId));
  }, [diagramId]);

  useEffect(() => {
    if (!isLoading && diagramXML) {
      if (viewerRef.current) {
        viewerRef.current.destroy();
      }

      viewerRef.current = new BpmnViewer({
        container: containerRef.current,
        readOnly: true,
      });

      viewerRef.current.importXML(diagramXML, (err: any) => {
        if (err) {
          console.error("Error rendering BPMN diagram", err);
        } else {
          if(viewerRef?.current !== null) {
            const canvas = viewerRef?.current.get("canvas");
            canvas.zoom("fit-viewport");

          }
        }
      });
    }
  }, [isLoading, diagramXML]);

  useEffect(() => {
    return () => {
      if (viewerRef.current) {
        viewerRef.current.destroy();
      }
    };
  }, []);

  return <div ref={containerRef} style={{ height: "500px" }} />;
};

export default BpmnViewerComponent;
