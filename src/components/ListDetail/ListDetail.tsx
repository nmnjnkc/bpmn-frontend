import React from "react";

interface ListDetailProps {
  value: string | null | undefined | number;
}

const ListDetail: React.FC<ListDetailProps> = ({ value }) => {
  return (
    <div className="list-detail">
      <p>{value}</p>
    </div>
  );
};

export default ListDetail;
