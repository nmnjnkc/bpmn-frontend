import { FC, MouseEvent } from "react";

interface ButtonProps {
  name: string;
  clickHandler: () => void;
}

const Button: FC<ButtonProps> = ({ name, clickHandler }) => {
  const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
    clickHandler();
  };

  return <button onClick={handleClick}>{name}</button>;
};

export default Button;
