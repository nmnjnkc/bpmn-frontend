import Button from "../../components/Button/Button";
import { useNavigate } from "react-router-dom";
import { RoutePaths } from "../../utils/enums";
import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../redux/store";
import { selectDiagrams } from "../../redux/diagrams/diagrams.selectors";
import {
  getAllDiagrams,
  deleteDiagram,
} from "../../redux/diagrams/diagrams.slice";
import ListDetail from "../../components/ListDetail/ListDetail";
import { formatDate } from "../../utils/functions";

const LandingPage = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [delDiagram, setDelDiagram] = useState(true);

  useEffect(() => {
    if (delDiagram) {
      dispatch(getAllDiagrams());
      setDelDiagram(false);
    }
  }, [delDiagram]);

  const diagrams = useAppSelector(selectDiagrams);

  const navigateToCreate = () => {
    navigate(RoutePaths.CREATE_BPMN);
  };

  const navigateToDiagram = (id: string) => {
    navigate(`${RoutePaths.DIAGRAM_BPMN}/${id}`);
  };

  const deleteDiagramById = (id: string) => {
    dispatch(deleteDiagram(id));
    setDelDiagram(true);
  };

  return (
    <div>
      <br />
      <Button name={"Create diagram"} clickHandler={navigateToCreate} />
      <br />
      <br />
      <br />
      <ul>
        <li>
          <h4>File Name</h4>
          <h4>Author ID</h4>
          <h4>Upload Date</h4>
        </li>
        {diagrams?.length > 0 &&
          diagrams?.map((listItem, key) => (
            <li key={key}>
              <ListDetail value={listItem?.fileName} />
              <ListDetail value={listItem?.authorId} />
              <ListDetail value={formatDate(listItem?.uploadDate)} />
              <Button
                name={"View diagram"}
                clickHandler={() => navigateToDiagram(listItem?.fileId)}
              />
              <br />
              <br />
              <Button
                name={"Delete diagram"}
                clickHandler={() => deleteDiagramById(listItem?.fileId)}
              />
            </li>
          ))}
      </ul>
    </div>
  );
};

export default LandingPage;
