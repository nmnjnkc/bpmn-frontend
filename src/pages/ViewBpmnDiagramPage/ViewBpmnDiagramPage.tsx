import React from "react";
import BpmnViewerComponent from "../../components/BpmnViewer/BpmnViewer";
import { useParams } from "react-router-dom";

const YourComponent: React.FC = () => {
  const { id: diagramId } = useParams();

  return (
    <div>
      {typeof diagramId === "string" ? (
        <BpmnViewerComponent diagramId={diagramId} />
      ) : (
        <p>Invalid diagramId</p>
      )}
    </div>
  );
};

export default YourComponent;
