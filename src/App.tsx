import { Route, Routes } from "react-router-dom";
import { RoutePaths } from "./utils/enums";
import LandingPage from "./pages/LandingPage/LandingPage";
import CreateBpmnPage from "./pages/CreateBpmnPage/CreateBpmnPage";
import ViewBpmnDiagramPage from "./pages/ViewBpmnDiagramPage/ViewBpmnDiagramPage";
import UpdateBpmnDiagramPage from "./pages/UpdateBpmnDiagramPage/UpdateBpmnDiagramPage";

function App() {

  return (
    <>
      <Routes>
        <Route path={RoutePaths.INDEX} element={<LandingPage />} />
        <Route path={RoutePaths.CREATE_BPMN} element={<CreateBpmnPage />} />
        <Route path={`${RoutePaths.DIAGRAM_BPMN}/:id` } element={<ViewBpmnDiagramPage />} />
        <Route path={`${RoutePaths.UPDATE_BPMN}/:id` } element={<UpdateBpmnDiagramPage />} />
      </Routes>
    </>
  );
}

export default App;
